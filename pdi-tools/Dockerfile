FROM ubuntu-debootstrap:trusty

MAINTAINER rjrivero <private@email.com>

# Java version
ENV JAVA_VERSION "7"

# Let the container know that there is no TTY
ENV DEBIAN_FRONTEND noninteractive

# Install dependencies.
# - curl and unzip to download the files
# - bash for the pentaho user shell scripts
# - xvfb to support headless reports
# - openjdk to run the server. Version 7 makes spoon fail.
# - ca-certificates to allow connecting to the Pentaho marketplace servers
# - libwebkitgtk-1.0.0 and libxtst6 required by kettle
# - mysql to run a dev repository
#   (https://help.pentaho.com/Documentation/5.4/0F0/0J0/030)
#
RUN apt-get update && \
    apt-get install -y curl unzip bash xvfb \
        openjdk-${JAVA_VERSION}-jre-headless \
        ca-certificates \
        libwebkitgtk-1.0.0 libxtst6 libcanberra-gtk-module \
        mysql-server mysql-client \
        openssh-server xauth sudo && \
    apt-get clean

# Pentaho user passwd - required by sudo
ENV PASSWORD "letmein"

# Add pentaho user
RUN useradd -m -s /bin/bash -d /home/pentaho pentaho -G sudo && \
    echo    "pentaho:$PASSWORD" | chpasswd && \
    mkdir   -p /home/pentaho/.ssh && \
    mkdir   -p /app && \
    chown   pentaho:pentaho /home/pentaho/.ssh && \
    chown   pentaho:pentaho /app

# SSH login fix. Otherwise user is kicked off after login
# See https://docs.docker.com/examples/running_ssh_service/
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

# Disable mysql error log
RUN sed 's/^log_error/# log_error/' -i /etc/mysql/my.cnf

# Add repository description
ADD files/repositories.xml /home/pentaho/.kettle/repositories.xml
RUN chown -R pentaho:pentaho /home/pentaho/.kettle

# Pentaho Data Integration package, get from sourceforge
ENV PENTAHO_MAJOR "5.4"
ENV PENTAHO_MINOR "5.4.0.1-130"

# Mysql Java connector, get from 
# http://dev.mysql.com/get/Downloads/Connector-J
ENV MYSQL_CONNECTOR_MINOR "5.1.36"

# Add startup scripts
ADD files/install-pdi.sh /app/install-pdi.sh
ADD files/run.sh         /app/run.sh

# Save environment variables in the install script
RUN sed -i "s/%PENTAHO_MAJOR%/$PENTAHO_MAJOR/" /app/install-pdi.sh && \
    sed -i "s/%PENTAHO_MINOR%/$PENTAHO_MINOR/" /app/install-pdi.sh && \
    sed -i "s/%MYSQL_CONNECTOR_MINOR%/$MYSQL_CONNECTOR_MINOR/" /app/install-pdi.sh

# - data-integration: Volume for pentaho installation files
# - report-designer: Volume for pentaho Report Designer
# - mysql: Volume for mysql database
VOLUME ["/app/data-integration", "/app/report-designer", "/var/lib/mysql"]

# Run ssh server
EXPOSE 22
CMD ["/app/run.sh"]
