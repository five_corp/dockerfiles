#!/usr/bin/env bash

# Load environment variables and stuff
export PENTAHO_MAJOR=${PENTAHO_MAJOR:-%PENTAHO_MAJOR%}
export PENTAHO_MINOR=${PENTAHO_MINOR:-%PENTAHO_MINOR%}
export MYSQL_CONNECTOR_MINOR=${MYSQL_CONNECTOR_MINOR:-%MYSQL_CONNECTOR_MINOR%}

pushd .

if [ ! -f /app/data-integration/spoon.sh ]; then

  # Get pentaho PDI package
  echo "Getting Pentaho Data Integration"
  curl -L -o /tmp/pdi-ce.zip "http://downloads.sourceforge.net/project/pentaho/Data Integration/${PENTAHO_MAJOR}/pdi-ce-${PENTAHO_MINOR}.zip"

  # cd to /app volume and install it
  cd /app
  unzip /tmp/pdi-ce.zip
  rm -f /tmp/pdi-ce.zip

fi 

if [ ! -f /app/report-designer/report-designer.sh ]; then

  # Get report designer package
  echo "Getting Pentaho Report Designer"
  curl -L -o /tmp/prd-ce.zip "http://downloads.sourceforge.net/project/pentaho/Report Designer/${PENTAHO_MAJOR}/prd-ce-${PENTAHO_MINOR}.zip"

  # cd to /app volume and install it
  cd /app
  unzip /tmp/prd-ce.zip
  rm -f /tmp/prd-ce.zip

fi

if [ ! -f /app/data-integration/lib/mysql-connector-java-${MYSQL_CONNECTOR_MINOR}-bin.jar ] || [ ! -f /app/report-designer/lib/mysql-connector-java-${MYSQL_CONNECTOR_MINOR}-bin.jar ]; then

  # Get mysql java driver
  echo "Getting MySQL JDBC Driver"
  curl -L -o /tmp/mysql.tgz "http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-${MYSQL_CONNECTOR_MINOR}.tar.gz"

  # unzip in /tmp and move the jar to the library dir
  cd /tmp
  tar -xzvf /tmp/mysql.tgz
  mkdir -p /app/data-integration/lib
  cp -f mysql-connector-java-${MYSQL_CONNECTOR_MINOR}/mysql-connector-java-${MYSQL_CONNECTOR_MINOR}-bin.jar /app/data-integration/lib/
  mkdir -p /app/data-integration/lib
  cp -f mysql-connector-java-${MYSQL_CONNECTOR_MINOR}/mysql-connector-java-${MYSQL_CONNECTOR_MINOR}-bin.jar /app/report-designer/lib/
  rm -rf mysql-connector-java-${MYSQL_CONNECTOR_MINOR}
  rm -f  mysql.tgz

fi

echo "Creating Kettle repository"

# Create DI repository in local mysql instance
mysql -u root -e "create database kettle; \
grant all on kettle.* to 'kettle'@'127.0.0.1' identified by 'kettle';"

# Create sample database
mysql -u root -e "create database sampledata; \
grant all on sampledata.* to 'pentaho_admin'@'127.0.0.1' identified by 'password';"

popd
