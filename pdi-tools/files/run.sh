#!/usr/bin/env bash

# Avoid error "missing privilege separation directory"
mkdir -p /var/run/sshd

# Run ssh server
echo "Starting ssh server..."
/usr/sbin/sshd

# Fixing StartUp Problems with some DNS Situations and Speeds up the stuff
# http://www.percona.com/blog/2008/05/31/dns-achilles-heel-mysql-installation/
cat > /etc/mysql/conf.d/mysql-skip-name-resolv.cnf <<EOF
[mysqld]
skip_name_resolve
EOF

# fix permissions and ownership of /app/data-integration
mkdir -p -m 755 /app/data-integration
chown -R pentaho:pentaho /app/data-integration

# fix permissions and ownership of /var/lib/mysql
mkdir -p -m 700 /var/lib/mysql
chown -R mysql:mysql /var/lib/mysql

# fix permissions and ownership of /run/mysqld
mkdir -p -m 0755 /run/mysqld
chown -R mysql:root /run/mysqld

# initialize MySQL data directory
if [ ! -d /var/lib/mysql/mysql ]; then
  echo "Installing database..."
  mysql_install_db --user=mysql >/dev/null 2>&1
fi

# start mysql server
echo "Starting MySQL server..."
exec /usr/bin/mysqld_safe 2>&1

