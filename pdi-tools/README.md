Pentaho Data Integration environment
====================================

This container builds a usr environment to load and try Pentaho
Data Integration / ETL tool, Kettle.

To build this container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd pdi-tools
docker build -t pdi-tools:master .
```

To run it:

```
docker run -d --name pdi-tools -P pdi-tools:master
```

Version
-------

Software versions installed are specified in the Dockerfile:

  - Java JRE version:
    - JAVA_VERSION: "7"
  - Pentaho community edition version:
    - PENTAHO_MAJOR: "5.4"
    - PENTAHO_MINOR: "5.4.0.1-130"
  - Mysql JDBC connector:
    - MYSQL_CONNECTOR_MINOR: "5.1.36"

Volumes
-------

  - /app/data-integration: The volume where Kettle will be installed. If it is empty, a fresh installation is performed to that path.
  - /app/report-designer: A volume intended for report templates.ç
  - /var/lib/mysql: Kettle repository database.

Ports
-----

The only published port is **SSH port 22**.

Usage
-----

Once you have started the container, you can ssh into it with username
**pentaho** and password **letmein**:

```
# Run the container
docker run -d --name pdi-tools -P pdi-tools:master

# Get container exposed port
docker inspect --format '{{ .Config.ExposedPorts }}' pdi-tools

# SSH and forward X. Password *letmein*
ssh -Y -l pentaho localhost -p <port>

# Install PDI, if not installed yet
/app/install_pdi.sh

# Start Spoon!
/app/data-integration/spoon.sh &
```

Simplified usage
----------------

To simplify container startup and login, you may want to:

  - Always map the same exposed port (say 2222) to internal port 22,
  - Mount data or host volumes to /app/data-integration, app/report-designer
    and /var/lib/mysql,
  - Mount your ssh key to /home/pentaho/.ssh/authorized_keys to allow for
    passwordless SSH.

In order to ease all these tasks, a helper script is included in **workspace/run.sh**. If you run it, it will:

  - Create *data-integration*, *report-designer* and *mysql* folders in the current directory,
  - Create / Start the container with the volumes and your public key mounted,
  - Expose the container's SSH port to port 2222 in the host.
  - log you into the container

So you can just run:

```
# Create the container, volumes, and log into the container without password
cd workspace
./run.sh

# Install PDI, if not installed yet
/app/install_pdi.sh

# Start Spoon!
/app/data-integration/spoon.sh &
```

