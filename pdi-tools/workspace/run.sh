#!/usr/bin/env bash

export CWD=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Remove previous instance, if any
docker stop pdi
docker rm   pdi

# Create directories if they do not exist
mkdir -p "$CWD/data-integration"
mkdir -p "$CWD/report-designer"
mkdir -p "$CWD/mysql"

# Launch new instance
docker run -d --name pdi \
       -p 2222:22 \
       -v "$CWD/data-integration:/app/data-integration" \
       -v "$CWD/report-designer:/app/report-designer" \
       -v "$CWD/mysql:/var/lib/mysql" \
       -v "$HOME/.ssh/id_rsa.pub:/home/pentaho/.ssh/authorized_keys" \
       pdi-tools

# Allow the ssh service to start
sleep 5

# Remove previously cached keys
ssh-keygen -f "$HOME/.ssh/known_hosts" -R [localhost]:2222

# Run ssh client
exec ssh -l pentaho -Y localhost -p 2222
