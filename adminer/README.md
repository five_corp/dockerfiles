ADminer container
=================

This is a simple extension to [clue/adminer]() container,
that adds PHP libraries required to support mongo.

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/adminer
docker build -t adminer .
```

To run:

```
docker run --rm -p 80:80 --name adm adminer
```

The container exposes  the **HTTP port, 80** by default.
