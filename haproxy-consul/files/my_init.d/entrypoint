#!/bin/sh

if [ -z "$CONSUL_NODE" ]
then
  echo "Missing CONSUL_NODE env var"
  exit -1
fi

# Escape a string for a sed replace pattern. See:
# http://stackoverflow.com/questions/407523
CONSUL_KEY=`echo "${CONSUL_KEY:-haproxy}" | sed -e 's/[\/&]/\\&/g'`
LOG_HOST=`echo "${LOG_HOST:-127.0.0.1}"   | sed -e 's/[\/&]/\\&/g'`
STATS_SERVICE=`echo "${STATS_SERVICE:-stats.}"   | sed -e 's/[\/&]/\\&/g'`

# Ilegal option, if running this script with "runit"
# set -eo pipefail

# if PEM file does not exist, create self signed certificate
if ! [ -f "/opt/haproxy/pem" ]; then
    openssl req -new -nodes -x509 \
         -subj "/C=ES/ST=Madrid/L=Madrid/O=IT/CN=`hostname`" \
         -days 3650 \
         -keyout /opt/haproxy/key \
         -out /opt/haproxy/crt \
         -extensions v3_ca
    cat /opt/haproxy/crt /opt/haproxy/key > "/opt/haproxy/pem"
fi

# Replace placeholders in consul-template files
for i in /etc/consul-template/*.hcl /etc/consul-template/*.ctmpl; do
  sed -i "s/\$CONSUL_NODE/$CONSUL_NODE/g;s/\$CONSUL_KEY/$CONSUL_KEY/g;s/\$STATS_SERVICE/$STATS_SERVICE/g;s/\$LOG_HOST/$LOG_HOST/g;" "$i"
done
