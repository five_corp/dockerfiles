Pentaho-BI container
====================

Minimal Pentaho BI Community Edition container with the default installation
and configuration (i.e. using the bundled tomcat and hsql database).

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/pentaho-server-min

# To build the x86 version
docker build -t pentaho-server-min .

# To build the armhf version
docker build -t pentaho-server-min -f Dockerfile.armhf .
```

To run:

```
docker run --rm -p 8080:8080 \
           -v /opt/pentaho/biserver-ce:/opt/biserver-ce \
           --name pentaho pentaho-server-min
```

The container exposes **port 8080**.

Environment variables
---------------------

If the instance is to be run behind a reverse proxy, two environment variables
must be provided:

  - PROXY_PORT: port number of the proxy.
  - PROXY_SCHEME: scheme used by the proxy.

Typically, if deploying behind a ssl proxy, the values of these environment
variables should be:

  - PROXY_PORT=443
  - PROXY_SCHEME=https

Volumes
-------

Pentaho runs from directory **/opt/biserver-ce**. You must:

  - Download the Pentaho Business Analytics Platform - Community Edition
    software from http://community.pentaho.com
  - Unzip it somewhere in your host server, say **/opt/pentaho**
  - Mount the resulting **/opt/pentaho/biserver-ce** folder to the container,
    in the **/opt/biserver-ce** path.

```
```

