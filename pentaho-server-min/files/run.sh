#!/bin/bash

cd /opt/biserver-ce || exit -1

# Fix an issue with missing validation file in Pentaho
# see http://jira.pentaho.com/browse/BISERVER-11746
if ! [ -f tomcat/webapps/pentaho/WEB-INF/classes/validation.properties ]; then
    touch tomcat/webapps/pentaho/WEB-INF/classes/validation.properties
fi
if ! [ -f tomcat/bin/validation.properties ]; then
    touch tomcat/bin/validation.properties
fi

# Configure CATALINA_OPTS for a headless system.
# Also restrict java to 1G memory (this is just a test environment...)
# and remove manual GC interval (-Dsun.rmi.dgc.server.gcInterval=3600000
# -Dsun.rmi.dgc.client.gcInterval=3600000)
sed -i 's/CATALINA_OPTS=.*/CATALINA_OPTS="-Djava.awt.headless=true -Xms1024m -Xmx2048m -XX:MaxPermSize=256m"/;' ./start-pentaho.sh

# Prepare the server to run behind a proxy.
# See http://wiki.bizcubed.com.au/xwiki/bin/view/Pentaho+Tutorial/NGiNX+Reverse+SSL+Proxy+for+Pentaho
if [ "x$PROXY_PORT" != "x" ]; then
    # Once the proxyPort is defined, do not define it again.
    # Otherwise restarting the container breaks
    if [ ! grep -q proxyPort tomcat/conf/server.xml ]; then
        sed -i "s/<Connector URIEncoding=\"UTF-8\" port=\"8080\" protocol=\"HTTP\/1.1\"/<Connector URIEncoding=\"UTF-8\" port=\"8080\" protocol=\"HTTP\/1.1\" proxyPort=\"$PROXY_PORT\" scheme=\"$PROXY_SCHEME\"/" tomcat/conf/server.xml
    fi
fi

echo "OK" | ./start-pentaho.sh
#/home/pentaho/biserver-ce/administration-console/startup.sh
exec tail -f ./tomcat/logs/catalina.out
