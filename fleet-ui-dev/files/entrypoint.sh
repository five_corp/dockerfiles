#!/bin/bash

export DOCKER_IMAGE_NAME=${DOCKER_IMAGE_NAME:-"fleet-ui"}
export DOCKER_IMAGE_VERSION=${DOCKER_IMAGE_VERSION:-"latest"}

# Assert the docker socket is there
if ! [ -S /var/run/docker.sock ]; then
    echo "Error: /var/run/docker is not a socket";
    exit -1;
fi

# add go user to the docker group
export DOCKER_GROUP=`stat -c "%g" /var/run/docker.sock`
groupadd -g "$DOCKER_GROUP" docker
usermod  -G "$DOCKER_GROUP" go

# Run the rest of the commands as the "go" user
sudo -u go GOPATH=/go /bin/bash <<EOF
  cd /go/src/github.com
  git clone https://github.com/rjrivero/fleet-ui.git fleet-ui
  cd fleet-ui
  ./build.sh $DOCKER_IMAGE_VERSION $DOCKER_IMAGE_NAME
EOF
