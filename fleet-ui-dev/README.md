GO development container
========================

Container intended as a build environment for the [fleet-ui](http://fleetui.com/) web interface to fleet. It contains all the dependencies and libraries
required to compile the application.

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/fleet-ui-dev
docker build -t fleet-ui-dev .
```

To run:

```
docker run --rm \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /usr/bin/docker:/usr/bin/docker \
    -e DOCKER_IMAGE_NAME=fleet-ui \
    -e DOCKER_IMAGE_VERSION=latest \
    fleet-ui-dev
```

Environment variables
---------------------

The container accepts the following environment variables:

  - DOCKER_IMAGE_NAME: Docker name for the image to build.
  - DOCKER_IMAGE_VERSION: Docker tag for the image to build.

