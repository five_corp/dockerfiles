GO development container
========================

Container intended as a generic GO build environment. It includes the
most common libs and tools for go development, so you don't have to include
them in your service container or install them in your system, if you are not
a GO developer. Just run the GO build container, compile your apps statically,
and put the result in your actual service container.

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/gobuild
docker build -t gobuild .
```

To run:

```
docker run --rm -it -v /my/workspace:/build gobuild
```

Environment variables
---------------------

The container does not use any environment variable.

Volumes
-------

The container's workdir is set to **/build**. Tipically you would mount there
your source code, in order to compile it.

