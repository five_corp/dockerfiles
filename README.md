# Dockerfiles

Various dockerfiles for microservices.

  - [HAProxy-confd](haproxy-confd/README.md): haproxy load balancer with etcd configuration
  - [fleet-ui-dev](fleet-ui-dev/README.md): helper container to build the [fleet-ui](https://github.com/rjrivero/fleet-ui) fleet interface (not working yet!).
  - [Jasper-server-min](jasper-server-min/README.md): minimal test-only JasperReports Server.
  - [Pentaho-server-min](pentaho-server-min/README.md): minimal test-only Pentaho BI Server.
  - [Lab-devel](lab-devel/README.md): development environment with embedded Cloud9 IDE.
  - [ADminer](adminer/README.md): Simple Database IDE supporting multiple database flavours.
  - [GObuild](gobuild/README.md): GO Development environment in a container.
