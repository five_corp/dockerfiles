JasperReports-Server container
==============================

Minimal JasperReports-Server container with the default installation
and configuration (i.e. using the bundled tomcat and postgresql database).

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/jasper-server-min
docker build -t jasper-server-min .
```

To run:

```
docker run --rm -p 8080:8080 --name jasper jasper-server-min
```

The container exposes **port 8080**. The web app is located
under the /jasperserver path.
