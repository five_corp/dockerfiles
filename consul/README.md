Consul container
================

Container for the [Consul](https://consul.io/) discovery service agent.
This container uses a compiled version of consul agent and ui built with
the bundled Makefile, so you can deploy this container in ARM provided you
run the Makefile in an ARM environment to build the executables.

To build the container:

```
# Get the source
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/consul

# Make the consul binary
make
# Or if you do not have the GO tools and libraries installed,
# use the gobuild container in my helper Dockerfiles repo
# (https://bitbucket.org/five_corp/dockerfiles)
docker run --rm -it -v `pwd`:/build gobuild make

# Build the container (X86 version)
docker build -t consul .
# Build the container (ARM version)
docker build -t consul -f Dockerfile.armhf .
```

To run:

```
docker run --rm -it -p 8500:8500 -e ROLE=server consul
```

Then open a browser to http://localhost:8500 and use the consul UI.
for full featured service running, see [Helping features](#Helping-features).

Environment variables
---------------------

The container uses a bunch of environment variables:

  - **GOMAXPROCS**: Number of max GO processes, defaults to 2.
  - **IMAGE**: fully qualified name of the image (e.g. including repo and
    tags), for use with cmd:run (See [Helping features](#Helping-features))
  - **ROLE**: Set *-e ROLE=server* to have the consul agent run as a server.
  - **ADVERTISE**: External IP address, reachable by any other consul cluster
    members, that this agent will advertise. Defaults to container's eth0.
  - **DC**: DataCenter name, defaults to *dc1*.
  - **DOMAIN**: Consul domain, defaults to *consul.*.
  - **BOOTSTRAP**: Number of servers required to bootstrap the cluster,
    defaults to *1*.
  - **JOIN**: Comma-separated IP addresses of other members to join.
  - **RECURSOR**: Comma-separated list of external DNS servers, if you want
    Consul to resolve queries for external domains. Defaults to Google
    DNS servers (8.8.8.8, 8.8.4.4).

Most are optional. With the default values, the container just starts a
single-node cluster with only one server in *dc1* DataCenter, serving the
*consul.* domain.

Volumes
-------

Consul data-dir is set to **/data**, so mount a volume there if you want
your data to be persisted.

Ports
-----

The Consul service uses
[quite a few ports](https://www.consul.io/docs/agent/options.html#ports):

  - 8600: The DNS server (tcp and udp).
  - 8500: The HTTP API.
  - 8400: The RPC endpoint.
  - 8301: The Serf LAN port (tcp and udp).
  - 8302: The Serf WAN port (tcp and udp).
  - 8300: The RPC address.

Helping features
----------------

To help with ptoviding the correct container name and port mappings, the
**start** script has a useful feature to generate the docker run command
line. Just cull the script with the **cmd:run** argument:

```
> docker run --rm -it consul start cmd:run

# Edited for clarity
eval docker run --name consul -h $HOSTNAME \
     -e ROLE= -e BOOTSTRAP=1 -e RECURSOR=8.8.8.8,8.8.4.4 \
     -e DC=dc1 -e DOMAIN=consul. -e ADVERTISE= -e JOIN= \
     -p 8300:8300 -p 8301:8301 -p 8301:8301/udp -p 8302:8302 \
     -p 8302:8302/udp -p 8400:8400 -p 8500:8500 -p 172.17.42.1:53:8600 \
     -p 172.17.42.1:53:8600/udp \
     consul start
```

You can pass any of the supported environment variables to the container and
it will mirror then in the generated command line. It will also forward
to the docker command line any further argument given to *start*:

```
> docker run --rm -it consul -e ROLE=server start cmd:run -d -v /consul:/data

# Edited for clarity
eval docker run --name consul -h $HOSTNAME \
     -e ROLE=server -e BOOTSTRAP=1 -e RECURSOR=8.8.8.8,8.8.4.4 \
     -e DC=dc1 -e DOMAIN=consul. -e ADVERTISE= -e JOIN= \
     -p 8300:8300 -p 8301:8301 -p 8301:8301/udp -p 8302:8302 \
     -p 8302:8302/udp -p 8400:8400 -p 8500:8500 -p 172.17.42.1:53:8600 \
     -p 172.17.42.1:53:8600/udp \
     -d -v /consul:/data start
```

So the easier way to start the container is to run a subshell with the
resulting command:

```
> $(docker run --rm -it consul -e ROLE=server start cmd:rum --rm -it)
```

If the image name is not "consul", then add the **IMAGE** environment
variable to the command:

```
> export CONSUL_IMG=myrepo/consul
> $(docker run --rm -it $CONSUL_IMG -e IMAGE=$CONSUL_IMG -e ROLE=server start cmd:rum --rm -it)
```
