#!/bin/bash

# eth0 IP address
export CLIENT_ADDR=`hostname -i`

# Default values
export IMAGE=${IMAGE:-consul}
export BOOTSTRAP=${BOOTSTRAP:-1}
export RECURSOR=${RECURSOR:-8.8.8.8,8.8.4.4}
export DC=${DC:-dc1}
export DOMAIN=${DOMAIN:-consul.}

cmd-run() {
    # Generate a docker run string

    # Docker Bridge IP and External IP address
    export BRIDGE_IP="$(ip route | awk '/^default/{print $3}')"
    export EXTERNAL=""

    if [ ! -z "$ADVERTISE" ]; then
        EXTERNAL="$ADVERTISE:"
    fi

    echo -n eval docker run --name consul -h \$HOSTNAME \
    -l SERVICE_IGNORE=true \
    -e ROLE=${ROLE} \
    -e BOOTSTRAP=${BOOTSTRAP} \
    -e RECURSOR=${RECURSOR} \
    -e DC=${DC} \
    -e DOMAIN=${DOMAIN} \
    -e ADVERTISE=${ADVERTISE} \
    -e JOIN=${JOIN} \
    -p ${EXTERNAL}8300:8300 \
    -p ${EXTERNAL}8301:8301 \
    -p ${EXTERNAL}8301:8301/udp \
    -p ${EXTERNAL}8302:8302 \
    -p ${EXTERNAL}8302:8302/udp \
    -p ${EXTERNAL}8400:8400 \
    -p ${EXTERNAL}8500:8500 \
    -p ${BRIDGE_IP}:53:8600 \
    -p ${BRIDGE_IP}:53:8600/udp \
    $@ ${IMAGE} start
}

cmd-start() {

    # If role server is defined, prepend an '-'
    if [ ! -z "$ROLE" ]; then
        export ROLE="-$ROLE"
    fi

    # Separate the recursors in different -recursor parameters
    RECURSOR=`echo $RECURSOR | sed 's/,/ -recursor /g'`
    RECURSOR="-recursor $RECURSOR"

    # Separate the join addresses in diferent -join parameters
    if [ -z "$JOIN" ]; then
        BOOTSTRAP="-bootstrap-expect ${BOOTSTRAP}"
    else
        BOOTSTRAP=""
        JOIN=`echo $JOIN | sed 's/,/ -join /g'`
        JOIN="-join $JOIN"
    fi

    # Set the client address to the IP address of eth0
    export ADVERTISE=${ADVERTISE:-$CLIENT_ADDR}

    # Delegate to the consul process
    export ARGUMENTS="agent ${ROLE} \
         -config-file /etc/consul.json
         -dc ${DC} -domain ${DOMAIN} \
         -client ${CLIENT_ADDR} \
         -advertise ${ADVERTISE} \
         ${RECURSOR} ${BOOTSTRAP} ${JOIN}"
    echo "STARTING /usr/bin/consul ${ARGUMENTS}"
    exec /usr/bin/consul ${ARGUMENTS}
}

set -eo pipefail
case "$1" in
    cmd:run)    shift; cmd-run $@;;
    *)          cmd-start;;
esac
