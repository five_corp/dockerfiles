Development container with embedded Cloud9 editor
=================================================

Heavy debian-based container with a rather full set of tools required
for node.js development:

  - Latest stable node.js version,
  - Build-essentials and libs to support app building,
  - MongoDB command line client tools,
  - The [Cloud9 SDK](https://github.com/c9/core),
  - The [Strongloop SDK](https://strongloop.com).
  - Ngnix front-end for serving dynamic and statoc content.

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/lab-devel
docker build -t lab-devel .
```

To run:

```
docker run --rm -e DEV_PASSWD=passwd -e APP_NAME=dev -e PROXY_HEADERS=new -p 8080:8080 -p 8081:8081 -p 8082:8082 --name dev lab-devel
```

The container exposes three ports:

  - **8080** is the nginx port. Nginx publishes any content under */opt/www*.
  - **8081** is the Cloud9 port.
  - **8082** is intended for Strongloop arc console.

Environment Variables
---------------------

The container uses the following Environment variables:

  - **DEV_PASSWD** sets the password for the Cloud9 **admin** user.
  - **APP_NAME** sets the name for the loopback.io app that is bootstrapped if */opt/app* is empty.
  - **PROXY_HEADERS** defines what nginx has to do regarding to proxy headers:
    - If you run this service behind a reverse proxy, set this variable to **PROOXY_HEADERS=keep**. This way nginx will keep the http headers created by the previous proxy, like "Host", "X-Forwarded-For", "X-Forwarded-Proto", and so on.
    - If you run this service without reverse proxy, leave **PROXY_HEADERS=new** so that nginx creates all those headers by itself.

Volumes
-------

The container uses the following volumes:

  - /opt/www: DocumentRoot for the nginx server /static location.
  - /opt/app: Convenience path to upload your node.js source.

Nginx Settings
--------------

The nginx server is configured to listen to port **8080** and serve all
request from **/opt/www**. The service runs with "*www-data:www-data*" user
and group credentials.

Requests for missing URLs are proxied to **localhost:3000**. Start your node
application listening at port 3000 and the nginx server will forward to it
any request it cannot serve locally.

Beware because **in case of conflict, local resources win**. I.e. if your
node application exports an URL which is also available in /opt/www, then
the local resource will be served.

The proxy behaviour depends on the value of the environment variable **PROXY_HEADERS**:

    - If *PROXY_HEADERS == new*, relevant proxy headers like X_FORWARDED_FOR, X_FORWARDED_PROTO, etc, are added by the nginx server in the container.
    - If *PROXY_HEADERS == keep*, nginx just passes along any header received from the client. You should use this value if the service is already behind a reverse proxy.

Cloud9 Settings
---------------

Cloud9 editor is accesible through port **8081**. The user name is **admin**,
and the default password is **passwd**. The desired password can be passed as
an Environment variable to docker (*-e DEV_PASSWD=whatever*).

The SDK runs as root, and using the credentials mentioned above gives you root
access to the container.
It is a devel environment, after all. Use at your own risk.

ARC server
----------

The image includes strongloop tools and the ARC console, started from /opt/app and listening at port 8082. it doesn't requiere any credentials.

If there is no project at the */opt/app* path when the container is started, a new project is bootstarted with **slc loopback:app "$APP_NAME"**.

Development workflow
--------------------

This container is intended for quick and dirty development. The expected
workflow for you to use it is:

  - Login to the cloud9 SDK instance in port 8081.
  - Use the terminal application available in the IDE to:
    - apt-get install your libs,
    - git clone your repos,
    - npm install your packages,
    - curl / wget your resources,
    - and anything else you need.
  - Use the drag and drop file upload tool in the IDE to:
    - upload static content to /opt/www,
    - upload your apps' code to /opt/app,
    - put any other resource you need in place.
  - Start your service from the terminal application, listening at port 3000.
  - Connect to the nginx server listening at port 8080, and enjoy
    having static content served from /opt/www and dynamic content proxied
    to your app.
  - Use the IDE to edit, tweak and improve your files.

Fleet units
-----------

The repo includes several
[fleet](https://coreos.com/using-coreos/clustering/) units to ease launching
instances of the container in a CoreOS cluster:

  - **lab-devel@.service** is the service unit. It spawns an instance of
    the lab-devel container in the cluster.
  - **lab-devel-disc:8080@.service** is the discovery unit for the nginx
    service. It announces the instance to the load balancer.
  - **lab-devel-disc:8081@.service** is the discovery unit for the Cloud9
    service. It announces the instance to the load balancer.
  - **lab-devel-disc:8082@.service** is the discovery unit for the ARC
    service. It announces the instance to the load balancer.

These files are tuned to a very specific environment, so they just won't
work anywhere; they are intended for a cluster build with the
Ansible scripts [here](https://bitbucket.org/five_corp/labinfra). So read
the documentation there for a further understanding. There are a few
variables that you can tweak easily in the **lab-devel@.service** unit, though:

  - Look for the line containing *Environment="DEV_PASSWORD=passwd"* and change *passwd* by the password you want to use to access the Cloud9 IDE.
  - Look for the line containing *Environment="APP_NAME=dev"* and change *dev* by the name you want to assign to the loopback.io app.
  - Look for the line containing *MachineMetadata=persistence=1* and replace *1* with the number of the worker you want to pin the instance to.

