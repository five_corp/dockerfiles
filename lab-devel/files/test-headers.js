/* Simple node.js app to echo the HTTP headers we are receiving.
 * Just to make sure reverse proxies and stuff are adding the headers
 * as they are supposed to do.
 *
 * Usage: node test-headers.js [port number]
 *
 * The service listens in port 3000 by default.
 */
var http = require('http');
var args = process.argv.slice(2);

// Simply echo the request headers
var srv = http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/json'});
  res.end(JSON.stringify(req.headers));
});
srv.listen(args[0] || 3000);
