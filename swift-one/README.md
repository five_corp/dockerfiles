Openstack Swift Server
=========================

Minimal Openstack Swift + Keystone servce (Liberty versions) with MySQL
backend and otherwise basic installation and configuration options.

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/swift-one

# To build the x86 version
docker build -t swift-one .
```

To run:

```
docker run --rm -p 5000 -p 8080 -p 35357 \
           -v </path/to/settings>:/etc/default/swift \
           -v </path/to/storage>:/srv/node/tank \
           --name swift-one --hostname swift-one swift-one
```

The environment is preconfigured with:

  - Two services and API endpoints: Identity and Object Storage.
  - One Region, **RegionOne**.
  - Three projects: **admin**, **service** and **demo**.
  - Two roles: **admin** and **user**.
  - Three users: **admin**, **swift** and **demo**.

The demo user credentials are stored in /root/demo-openrc.sh . You can use
Keystone / Swift by retrieving that file and then:

```
source demo-openrc.sh
swift stat 
swift upload   container1 FILE
swift list     container1
swift download container1 FILE
```

Exposed ports
-------------

The container exposes **ports 5000, 8080 and 35357**.

  - Ports 5000 and 35357 belong to the identity service (Keystone).
  - Port 8080 belongs to Swift.

Volumes
-------

The container requires two volumes:

  - **/srv/node/tank**, which is the data volume used to store the objects. This volume is required, and the Swift service will not start without it.
  - **/etc/default/swift**, which is a configuration file that can export the following environment variables:
    - **SERVER_NAME**: The DNS name of the server. It is required for operation. If not provided, the runtime host name (*$HOSTNAME*) is assumed.
    - **DEMO_PASSWORD**: Password of the demo user in Keystone. Defaults to "Changeme".
    - **OS_PASSWORD**: Password of the admin user in Keystone. Defaults to "Changeme"

The configuration file is only honored the first time the container boots. Once
the service has been initialized, the server name and passwords are fixed in
the database and API endpoints and can only be changed manually.

Using from loopback
-------------------

This container was developed to be used in [Strongloop's Loopback](https://strongloop.com/node-js/loopback-framework/)  development environments. Instructions to use the loopback's storage provider are available [in their manual](https://docs.strongloop.com/display/public/LB/Storage+component); as a quick start:

  - Install loopback-component-storage

```
npm install loopback-component-storage
```

  - Add the storage as a datasource, in **server/datasources.json**:

```
  "swift": {
    "username": "demo",
    "password": "<Demo User Password>",
    "name": "swift",
    "connector": "loopback-component-storage",
    "provider": "openstack",
    "authUrl": "http://<Swift Server Name>:5000",
    "region": "RegionOne"
  }
```

  - Create an empty Container model, in **common/models/container.js**

```
container.json:

{
  "name": "Container",
  "base": "Model",
  "properties": {},
  "validations": [],
  "relations": {},
  "acls": [],
  "methods": {}
}

container.js:

module.exports = function(Container) {

};
```

  - Set the storage datasource as the model's backing in **server/model-config.json**:

```
  "Container": {
    "dataSource": "swift",
    "public": true,
    "$promise": {},
    "$resolved": true
  }
```

And voila, you get the */api/Containers* REST endpoints.

