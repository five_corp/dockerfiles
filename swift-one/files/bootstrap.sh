#!/usr/bin/env bash

# Break build chain on error
set -eo pipefail

# Avoid problem with ssl "unable to write random state"
# See http://stackoverflow.com/questions/94445
export RANDFILE=/root/.rnd
export KEYSTONE_DBPASS=`openssl rand -hex 32`
export OS_TOKEN=`openssl rand -hex 20`

echo Generating random passwords and prefixes
echo ----------------------------------------
export RAND=`openssl rand -hex 32`
sed -i "s/%HASH_PATH_PREFIX%/${RAND}/g" /etc/swift/swift.conf
export RAND=`openssl rand -hex 32`
sed -i "s/%HASH_PATH_SUFFIX%/${RAND}/g" /etc/swift/swift.conf

echo Starting MySQL and waiting for socket to be ready
echo -------------------------------------------------
/usr/sbin/mysqld &
sleep 5

echo Creating DB user and granting privileges
echo ----------------------------------------
/usr/bin/mysql -u root <<EOF
CREATE DATABASE keystone;
GRANT ALL PRIVILEGES ON keystone.* TO 'keystone'@'localhost' \
  IDENTIFIED BY '${KEYSTONE_DBPASS}';
EOF

echo Storing DB Password in /etc/keystone/keystone.conf
echo --------------------------------------------------
sed -i "s/%KEYSTONE_DBPASS%/${KEYSTONE_DBPASS}/g" /etc/keystone/keystone.conf
sed -i "s/%OS_TOKEN%/${OS_TOKEN}/g" /etc/keystone/keystone.conf

echo Initializing Keystone Database
echo ------------------------------
/sbin/setuser keystone /usr/bin/keystone-manage db_sync

echo Shutting down MySQL Database
echo ----------------------------
/usr/bin/mysqladmin -u root shutdown

echo Adding temporary token to admin-openrc.sh, demo-openrc.sh
echo ---------------------------------------------------------
echo "export OS_TOKEN=\"${OS_TOKEN}\"" >> /root/admin-openrc.sh
echo "export OS_TOKEN=\"${OS_TOKEN}\"" >> /root/demo-openrc.sh
