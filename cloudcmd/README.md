Cloudcmd environment
====================

[Cloudcmd](http://cloudcmd.io/)-based web file browser.
Allows browsing, uploading and downloading files to a container filesystem.

To build the container:

```
git clone https://bitbucket.org/five_corp/dockerfiles.git
cd dockerfiles/cloudcmd
docker build -t cloudcmd .
```

To run:

```
docker run --rm -v /opt:/opt -p 8000:8000 --name cloudcmd cloudcmd
```

The container exposes port 8000. 

Environment Variables
---------------------

The container uses no environment variables.

Volumes
-------

The container uses just one volume, **/opt**. This path is exposed as the
root folder in the web browser.
